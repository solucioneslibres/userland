// Copyright 2019 Hackware SpA <human@hackware.cl>
// This file is part of "Hackware Userland" and licensed under the terms
// of the GNU Affero General Public License version 3, or (at your option)
// a later version. You should have received a copy of this license along
// with the software. If not, see <https://www.gnu.org/licenses/>.

import Vue from 'vue';

const dateTimeFormatter = new Intl.DateTimeFormat('es-CL', {
  year: 'numeric',
  month: 'numeric',
  day: 'numeric',
  hour: 'numeric',
  minute: 'numeric',
  hourCycle: 'h23',
});

const dateFormatter = new Intl.DateTimeFormat('es-CL', {
  year: 'numeric',
  month: 'numeric',
  day: 'numeric',
});

Vue.filter('datetime', value => dateTimeFormatter.format(new Date(value)));
Vue.filter('date', value => dateFormatter.format(new Date(value)));
