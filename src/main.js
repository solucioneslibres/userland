// Copyright 2019-2022 Hackware SpA <human@hackware.cl>
// This file is part of "Hackware Userland" and licensed under the terms
// of the GNU Affero General Public License version 3, or (at your option)
// a later version. You should have received a copy of this license along
// with the software. If not, see <https://www.gnu.org/licenses/>.

import '@fortawesome/fontawesome-free/css/all.css';

import Vue from 'vue';
import App from './App.vue';
import router from './router';
import Auth from './Auth';
import Fetcher from './Fetcher';
import './filters';

Vue.prototype.$fetcher = new Fetcher(process.env.VUE_APP_HAWESE_ENDPOINT);
Vue.prototype.$auth = new Auth(Vue.prototype.$fetcher);
Vue.prototype.$eventHub = new Vue(); // This looks like Vuex

Vue.config.productionTip = false;

Vue.prototype.$filters = {
  currency(amount, currency, explicit = false) {
    const currencyFormatter = new Intl.NumberFormat(undefined, {
      style: 'currency',
      currencyDisplay: 'narrowSymbol',
      currency,
    });
    return currencyFormatter.format(amount) + (explicit ? ` ${currency}` : '');
  },
};

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
