# Hackware Userland

Frontend interface to various Hackware web services (not neccesarily part of the Hawese project). This project is running live on https://userland.hackware.cl.

## Main goals

- Allow users to interact with the Hawese wallet and pay backends.

## Features

- As user I can add money to my wallet so I'm able to back the provision of my services, even in the future. In this view I can select my preferred payment method and the amount to be paid.
- As user I can see my wallet's current balance and owed transactions with expiration date (if there's any) in the same page where I can add money, so I can pay with all the relevant information available.
- As user I can lookup my wallet's transaction history so I can easily check this information whenever I want.

## Run it

### Requirements

- npm
- [hackware/hawese](https://git.hackware.cl/hawese)

### Installation

```sh
npm install # Installs dependencies
```

Depending on the enviroment you are working on, either `development` or `production` setup the `.env.environment.local` file with the `APP_VUE_HAWESE_ENDPOINT` environment variable corresponding to the endpoint of the [hackware/hawese](https://git.hackware.cl/hawese) backend your instance of this frontend will communicate with.

#### Development

```sh
npm run serve # Compiles and hot-reloads
# Or you can append options as host and port using the `-- --param value` syntax such as
npm run serve -- --host dev.userland.hackware.cl
npm run lint # Lint and fix
```


#### Production

```sh
npm run build # Compiles and minifies
```

## Copyright and licensing

Copyright 2019 [Hackware SpA](https://hackware.cl).

This project is licensed under the GNU Affero General Public License v3 or any later version.

Though crafted by me with Inkscape and subject to the same license as the rest of the project images on `public/img/payment_methods/` might be subject to trademark or related restrictions.

Other images from - or based on - [Unsplash](https://unsplash.com) or [Pixabay](https://pixabay.com).

### How to comply with license terms?

To comply with license terms you must provide the means to access the source code of your version of the software easily and free of charge to any person that has access to the software, even users which access through a network (i.e. web browser). I've provided an easy way to accomplish this requirement by adding a `#userland_sourcecode` link element on the sidebar which must always link to a clone of the source code you are running, if you have NOT made modifications to this proyect then it's OK to use the already provided URL.

Any modification or inclusion of this source code will inherit the same license, that is, it can't be sublicensed.

### Other licensing options

If you want to use the software but are afraid of or can't comply with the license terms we can arrange other license terms for your case. Though most probably I'll not accept if you don't provide your modifications to the software back.
